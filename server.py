from flask import Flask
from controllers.books_controller import books_api
from flask_cors import CORS


app = Flask(__name__)
app.register_blueprint(books_api, url_prefix="/api/books")

CORS(app)


@app.route('/', methods=["GET"])
def hello():
    return "Hello World"


if __name__ == '__main__':
    port: int = 8000
    print(f'app is running on port {port}')
    app.run(host='localhost', port=port)

