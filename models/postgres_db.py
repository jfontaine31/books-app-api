import psycopg2
from psycopg2.extras import RealDictCursor
from contextlib import contextmanager
from os import environ
from typing import Generator


class PostgresDB:
    def __init__(self):
        self.conn = None

    @contextmanager
    def get_cursor(self) -> Generator:
        conn = self.connect()
        cursor = conn.cursor(cursor_factory=RealDictCursor)
        yield cursor
        cursor.close()
        conn.commit()
        conn.close()

    def connect(self):
        try:
            return psycopg2.connect(
                host=environ['db_host'],
                database=environ['db_database'],
                user=environ['db_user'],
                password=environ['db_password']
            )
        except Exception as ex:
            print('there was an error connecting to the database', ex)
            raise ex

