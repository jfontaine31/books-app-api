from flask import Blueprint, jsonify, request
from models.postgres_db import PostgresDB
from psycopg2 import Error

from typing import List, Dict

books_api = Blueprint("books", __name__)

db = PostgresDB()


@books_api.route('/', methods=['GET'])
def get_all_books() -> List:
    rows: List = None
    try:
        with db.get_cursor() as cursor:
            cursor.execute("SELECT * FROM books;")
            rows: List[Dict] = cursor.fetchall()
    except Error as ex:
        print("error finding all books")
        return jsonify({'err': "books not found", "status_code": 404}), 404
    return jsonify({'data': rows, "status_code": 200}), 200


@books_api.route('/<int:book_id>', methods=['GET'])
def get_book(book_id: int) -> Dict:
    book = None
    try:
        with db.get_cursor() as cursor:
            cursor.execute("SELECT * FROM books WHERE id = %s;", (book_id,))
            book = cursor.fetchone()
            if not book:
                raise Error('not found')
    except Error as ex:
        print('error getting book', ex)
        return jsonify({'err': "Unable to find book", "status_code": 404}), 404
    return jsonify({'data': book, "status_code": 200}), 200


@books_api.route('/<int:book_id>', methods=['POST'])
def update_book(book_id: int):
    try:
        with db.get_cursor() as cursor:
            title = request.json['title']
            author = request.json['author']
            cursor.execute("UPDATE books SET title = %s, author = %s WHERE id = %s", (title, author, book_id))
    except Exception as ex:
        print('error  updating book', ex)
        return jsonify({'err': "Unable to update", "status_code": 400}), 400
    return jsonify({'data': f"book: {book_id} updated Successfully", "status_code": 200}), 200


@books_api.route('/', methods=["PUT"])
def insert_book():
    try:
        title = request.json['title']
        author = request.json['author']
        with db.get_cursor() as cursor:
            cursor.execute("INSERT INTO books (title, author) VALUES (%s, %s)", (title, author))
    except KeyError:
        return jsonify({'err': "Unable to insert", "status_code": 400}), 400
    return jsonify({'data': "Inserted Successfully", "status_code": 202}), 202


@books_api.route('/<int:book_id>', methods=["DELETE"])
def delete_book(book_id: int):
    print(f'deleting book {book_id}')
    try:
        with db.get_cursor() as cursor:
            cursor.execute("DELETE FROM books WHERE id = %s", (book_id,))
    except Error as ex:
        return jsonify({'err': "Error deleting record", "status_code": 404}), 404
    return jsonify({'data': "book deleted", "status_code": 200}), 200
